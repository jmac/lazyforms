<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */


namespace tests\unit\JMac\LazyForms;

use JMac\LazyForms\Form;
use JMac\LazyForms\Field;

require_once __DIR__ . '/../vendor/autoload.php';

class FormTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider rowConfigurationProvider
     */
    public function testDecodeRowConfiguration($rowKey, $rowValue, $expected)
    {
        $form = new Form();
        list($parsedData, $parsedTitle, $parsedValue) = $form->decodeRowConfiguration($rowKey, $rowValue);

        $this->assertEquals($expected[0], $parsedData);
        $this->assertEquals($expected[1], $parsedTitle);
        $this->assertEquals($expected[2], $parsedValue);
    }

    public function rowConfigurationProvider()
    {
        return [
            [
                '0',
                'name',
                ['name', null, null],
            ],
            [
                '0',
                ['first-name', 'last-name'],
                [['first-name', 'last-name'], null, null],
            ],
            [
                'first-name',
                'John McCann',
                ['first-name', null, 'John McCann'],
            ],
            [
                'Group Name',
                ['first-name', 'last-name'],
                [['first-name', 'last-name'], 'Group Name', null],
            ],
        ];
    }

    /**
     * @dataProvider formDecodeProvider
     */
    public function testFormDecode($input, $intended, $intendedTree)
    {
        $form = new Form();
        $form->populateFields($input);

        $this->assertEquals($intended, $form->fields);
        $this->assertEquals($intendedTree, $form->fieldTree);
    }

    public function formDecodeProvider()
    {
        return [
            //////////////////////////////////////////////////////////
            [
                [ 'username', ],
                [ 'username' => Field::create('username', null), ],
                [ Field::create('username', null), ],
            ],
            //////////////////////////////////////////////////////////
            [
                [   'username',
                    'first-name' => 'John McCann'
                ],
                [   'username' => Field::create('username', null),
                    'first-name' => Field::create('first-name', 'John McCann'),
                ],
                [
                    Field::create('username', null),
                    Field::create('first-name', 'John McCann'),
                ],
            ],
            //////////////////////////////////////////////////////////
            [
                [
                    'username',
                    'Grouped Fields' => [
                        'first-name',
                        'last-name' => 'McCann'
                    ]
                ],
                [
                    'username' => Field::create('username', null),
                    'first-name' => Field::create('first-name', null),
                    'last-name'  => Field::create('last-name', 'McCann'),
                ],
                [
                    Field::create('username', null),
                    'Grouped Fields' => [
                        Field::create('first-name', null),
                        Field::create('last-name', 'McCann'),
                    ]
                ],
            ],
            //////////////////////////////////////////////////////////
            [
                [
                    'username',
                    'Grouped Fields' => [
                        'first-name',
                        'last-name' => 'McCann'
                    ],
                    'password*',
                    [
                        'address-1' => 'Botley',
                        'address-2',
                    ]
                ],
                [
                    'username' => Field::create('username', null),
                    'first-name' => Field::create('first-name', null),
                    'last-name'  => Field::create('last-name', 'McCann'),
                    'password'  => Field::create('password*', null),
                    'address-1' => Field::create('address-1', 'Botley'),
                    'address-2' => Field::create('address-2', null),
                ],
                [
                    Field::create('username', null),
                    'Grouped Fields' => [
                        Field::create('first-name', null),
                        Field::create('last-name', 'McCann'),
                    ],
                    Field::create('password*', null),
                    [
                        Field::create('address-1', 'Botley'),
                        Field::create('address-2', null),
                    ]
                ],
            ],
            //////////////////////////////////////////////////////////
        ];
    }

}

?>

<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */
 

namespace tests\unit\JMac\LazyForms;

use JMac\LazyForms\Field;

require_once __DIR__ . '/../vendor/autoload.php';

class FieldTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider formDecodeProvider
     */
    public function testFormDecode($string, $resultingObject)
    {
        $field = new Field();
        $field->decodeOptions($string);

        foreach($resultingObject as $property => $intendedValue) {
            $this->assertEquals($intendedValue, $field->$property);
        }
    }

    public function formDecodeProvider()
    {
        return [
            [
                'first-name*|"Your first name"',
                [
                    'id'         => 'first-name',
                    'name'       => 'first-name',
                    'class'      => '',
                    'value'      => '',
                    'label'      => 'Your first name',
                    'type'       => 'text',
                    'isRequired' => true,
                ],
            ],
            [
                'last-name',
                [
                    'id'         => 'last-name',
                    'name'       => 'last-name',
                    'class'      => '',
                    'value'      => '',
                    'label'      => 'Last name',
                    'type'       => 'text',
                    'isRequired' => false,
                ],
            ],
            [
                'password-confirmation*|_Your password must contain at least 8 characters_',
                [
                    'id'         => 'password-confirmation',
                    'label'      => 'Password confirmation',
                    'type'       => 'password',
                    'isConfirmationOf' => 'password',
                    'helpText'   => 'Your password must contain at least 8 characters',
                    'isRequired' => true,
                ],
            ],
            [
                'mobile-phone-number|((+44) 7817 800686)|_Your mobile telephone number_',
                [
                    'label'       => 'Mobile phone number',
                    'type'        => 'text',
                    'placeholder' => '(+44) 7817 800686',
                    'helpText'    => 'Your mobile telephone number',
                    'isRequired'  => false,
                ],
            ],
            [
                'has-account',
                [
                    'type'       => 'checkbox',
                    'isRequired' => false,
                ],
            ],
            [
                'city|#Canterbury, Liverpool, Chelmsford, Oxford, Guildford, Nottingham#',
                [
                    'id'         => 'city',
                    'label'      => 'City',
                    'type'       => 'select',
                    'selectOptions' => ['Canterbury', 'Liverpool', 'Chelmsford', 'Oxford', 'Guildford', 'Nottingham']
                ],
            ],
        ];
    }

}

?>

<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */
 

namespace JMac\LazyForms;


class SelectFieldWriter extends FieldWriter
{
    public function drawInput(Field $field)
    {
        $field = $this->field;

        $html =
            '<select
                   id="'.$field->id.'"
                   name="'.$field->name.'"
                   '.$this->getInputClassAttr().'
                   tabindex="'.$this->nextTabIndex().'"
            >';

        foreach ($field->selectOptions as $key => $label) {
            $selected = ($key == $field->value) ? ' selected' : '';
            $html .= '<option value="'.$key.'" '.$selected.'>';
            $html .=    $label;
            $html .= '</option>';
        }

        $html .= '</select>';
        return $html;
    }
}

?>

<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */

namespace JMac\LazyForms;

class FormWriter
{
    /** @var Form $form */
    protected $form;

    public static function create(Form $form)
    {
        return new FormWriter($form);
    }

    public function __construct(Form $form)
    {
        $this->form = $form;
    }

    public function draw()
    {
        $this->tabIndex = 1;
        $form = $this->form;

        $html = '<form id="'.$form->id.'"
                    action="'.$form->action.'"
                    class="'.$form->class.'"
                    method="'.$form->method.'"
                >';

        $html .= $this->drawRows($form->fieldTree);
        $html .= $this->drawSubmitButton();
        $html .= "</form>";
        return $html;
    }

    /**
     * @author John McCann
     * @param Field|array $rows
     * @return string
     */
    protected function drawRows($rows, $level = 1)
    {
        $html = '';
        foreach ($rows as $key => $row) {
            if (is_array($row) && is_numeric($key)) {
                $html .= $this->drawInlineRow($row, $level++);
            } else if (is_array($row)) {
                $html .= '<h4 class="form-subtitle">'.$key.'</h4>';
                $html .= $this->drawRows($row, $level++);
            } else {
                $field = $row;
                $fieldWriter = FieldWriter::Create($field, $this->form->classPrefix, $this->form->labelSize, $this->form->inputSize);
                $html .= $fieldWriter->draw($field);
            }
        }
        return $html;
    }

    /**
     * @author John McCann
     * @param Field[] $rowData
     * @return string
     */
    protected function drawInlineRow($rowData)
    {
        $numColumns = count($rowData);
        $columnSize = 12 / $numColumns;

        if (!in_array($columnSize, array(1,2,3,4,6,12))) {
            return "Unsupported number of columns in inline form row!";
        }

        $html = '<div class="row">';
        foreach ($rowData as $field) {
            $fieldWriter = FieldWriter::Create($field, $this->form->classPrefix, $this->form->labelSize, $this->form->inputSize);

            $html .= '<div class="col-md-'.$columnSize.'">';
            $html .= $fieldWriter->draw($field);
            $html .= '</div>';
        }
        $html .= '</div>';
        return $html;
    }


    public function drawSubmitButton ()
    {
        $html = '<input type="submit" id="'.$this->form->id.'-submit-btn" class="btn btn-default btn-submit"/>';
        return $html;
    }

    public function drawSuccessMessage ()
    {
        return '<div class="'.$this->form->classPrefix.'success">Your form was successfully submitted.</div>';
    }
}

?>

<?php
/**
 * Class that writes out formatted html relating to a field object
 *
 * @author     John McCann
 */
 

namespace JMac\LazyForms;

class FieldWriter
{
    /** @var Field */
    protected $field;
    protected $classPrefix = 'form-';

    protected $inputSize;
    protected $labelSize;

    /**
     * Static factory method to create a class of the appropriate type
     *
     * @author John McCann
     * @param Field $field
     * @param       $classPrefix
     * @param       $labelSize
     * @param       $inputSize
     * @param int   $tabIndex
     * @return FieldWriter
     */
    public static function create(Field $field, $classPrefix, $labelSize, $inputSize, $tabIndex = 0)
    {
        switch ($field->type) {

            case 'text':
                return new FieldWriter($field, $classPrefix, $labelSize, $inputSize, $tabIndex);
            case 'select':
                return new SelectFieldWriter($field, $classPrefix, $labelSize, $inputSize, $tabIndex);
            case 'checkbox':
                return new CheckboxFieldWriter($field, $classPrefix, $labelSize, $inputSize, $tabIndex);
            default:
                return new FieldWriter($field, $classPrefix, $labelSize, $inputSize, $tabIndex);
        }
    }

    protected function __construct(Field $field, $classPrefix, $labelSize, $inputSize, $tabIndex = 0)
    {
        $this->field = $field;
        $this->classPrefix = $classPrefix;
        $this->labelSize = $labelSize;
        $this->inputSize = $inputSize;
        $this->tabIndex = $tabIndex;
    }

    /**
     * Returns the html text for the whole field group, including label & containing div
     * @author John McCann
     * @param Field $field
     * @return string
     */
    public function draw(Field $field)
    {
        $this->field = $field;

        return <<<HTML
            <div {$this->getGroupClassAttr()}>
                <label {$this->getLabelClassAttr()} for="{$field->id}">
                    {$this->getLabel()}
                </label>

                <div {$this->getInputWrapperClassAttr()}>
                    {$this->drawInput($field)}
                    {$this->getHelperElement()}
                    {$this->getErrorElement()}
                </div>

            </div>
HTML;
    }

    /**
     * Draws the input part of the field group
     * @author John McCann
     * @return string
     */
    public function drawInput()
    {
        $field = $this->field;

        $html =
            '<input type="'.$field->type.'"
                    id="'.$field->id.'"
                    name="'.$field->name.'"
                    '.$this->getInputClassAttr().'
                    value="'.$field->value.'"
                    '.$this->getPlaceholderAttr().'
                    tabindex="'.$this->nextTabIndex().'"
            >';
        return $html;
    }

    /**
     * Returns and increments the tabindex so that subsequent inputs can be automatically
     * drawn with sequential tab indexes
     * @author John McCann
     * @return int
     */
    public function nextTabIndex()
    {
        return $this->tabIndex++;
    }

    /**
     * Applies any formatting to the label and returns it
     * @author John McCann
     * @return string
     */
    protected function getLabel()
    {
        $label = $this->field->label;

        if ($this->field->isRequired) {
            $label .= '*';
        }

        return $label;
    }

    /**
     * Returns the css class attribute that is required on the containing form group div
     * @author John McCann
     * @return string
     */
    public function getGroupClassAttr()
    {
        $formGroupClass  = $this->classPrefix.'group';
        if ($this->field->hasError) {
            $formGroupClass .= ' has-error';
        }
        return 'class="'.$formGroupClass.'"';
    }

    /**
     * Returns the css class attribute that is required on the label
     * @author John McCann
     * @return string
     */
    protected function getLabelClassAttr()
    {
        $class = '';

        if ($this->labelSize) {
            // TODO: Frameworks act differentlly here (bootstrap / foundation etc..)
            $class .= "col-md-$this->labelSize ";
            $class .= "col-sm-$this->labelSize ";
            $class .= "col-xs-$this->labelSize ";
        }

        $class .= 'control-label';

        return 'class="'.$class.'"';
    }

    /**
     * returns the css class attribute that is required on the div that wraps the input element
     * @author John McCann
     * @return string
     */
    protected function getInputWrapperClassAttr()
    {
        $class = '';

        if ($this->inputSize) {
            // TODO: Frameworks act differentlly here (bootstrap / foundation etc..)
            $class .= "col-md-$this->inputSize ";
            $class .= "col-sm-$this->inputSize ";
            $class .= "col-xs-$this->inputSize ";
        }

        if ($this->field->hasError) {
            $class .= ' has-error error-'.$this->field->id;
        }
        return 'class="'.$class.'"';
    }

    /**
     * Returns the css class attribute that is required on the input element
     * @author John McCann
     * @return string
     */
    protected function getInputClassAttr()
    {
        $class = $this->classPrefix.'control '.$this->classPrefix.'field-'.$this->field->class;

        if ($this->field->hasError) {
            $class .= ' has-error error-'.$this->field->id;
        }
        return 'class="'.$class.'"';
    }


    /**
     * Returns the html placeholder attribute if set on the field
     * @author John McCann
     * @return string
     */
    public function getPlaceholderAttr()
    {
        if ($this->field->placeholder) {
            return 'placeholder="'.$this->field->placeholder.'"';
        }
        return '';
    }

    /**
     * Returns the help text element if help text has been set on the field
     * @author John McCann
     * @return string
     */
    public function getHelperElement()
    {
        return (isset($this->field->helpText))
            ? '<p class="help-block">'.$this->field->helpText.'</p>'
            : '';
    }

    /**
     * Returns the error text element if errors exist in the field
     * @author John McCann
     * @return string
     */
    private function getErrorElement ()
    {
        if (!empty($this->field->errors)) {
            $errorText = implode(', ', $this->field->errors);
            return '<div class="error-block">'.$errorText.'</div>';
        }
        return '';
    }

}

?>

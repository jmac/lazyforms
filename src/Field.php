<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */
 

namespace JMac\LazyForms;

//use JMac\Utils;

class Field
{
    public $id;
    public $name;
    public $class;
    public $value;
    public $label;
    public $type;
    public $placeholder;

    public $selectOptions;

    public $helpText;

    public $classPrefix = 'form-';
    public $isConfirmationOf;
    public $isRequired;
    public $validationTypes = array();

    public $hasError = false;
    public $errors;


    public static function create($data, $value = null)
    {
        $field = new Field();

        if ($value) {
            $field->value = $value;
        }

        if (!is_array($data)) {
            $field->decodeOptions($data);

        } else {
            foreach($data as $key => $value) {
                if (property_exists($field, $key)) {
                    $field->$key = $value;
                } else {
                    error_log("Couldn't initialise Field with property $key");
                }
            }
        }

        $field->class = $field->id;

        return $field;
    }

    const VALIDATE_EMAIL = 'email';
    const VALIDATE_PHONE = 'phone';
    const VALIDATE_PASSWORD = 'password';

    public function decodeOptions($string)
    {
        $validationTypes = array(
            self::VALIDATE_EMAIL    => array('email'),
            self::VALIDATE_PHONE    => array('phone'),
            self::VALIDATE_PASSWORD => array('password'),
        );

        $searchInputOptions = array(
            'password' => array('password'),
            'checkbox' => array('is-', 'has-'),
        );

        $chunks = explode('|', $string);

        for ($i = 0; $i < count($chunks); $i++) {

            $chunk = trim($chunks[$i]);

            if (empty($chunk)) {
                continue;
            }

            // First chunk is always the id
            if ($i == 0) {

                // Asterisk means field is required
                if (strpos($chunk, '*') !== false) {
                    $this->isRequired = true;
                    $chunk = str_replace('*', '', $chunk);
                }

                // Field ending in confirmation means it needs to match the similarly named field
                if (strpos($chunk, '-confirmation') !== false) {
                    $this->isConfirmationOf = str_replace('-confirmation', '', $chunk);
                }

                // Set validation types
                foreach($validationTypes as $validationType => $validationHints) {
                    foreach($validationHints as $hint) {
                        if (strpos ($chunk, $hint) !== false) {
                            $this->validationTypes[$validationType] = $validationType;
                        }
                    }
                }

                // Field Types
                $this->type = 'text';

                foreach($searchInputOptions as $inputType => $inputHints) {
                    foreach($inputHints as $hint) {
                        if (strpos($chunk, $hint) !== false) {
                            $this->type = $inputType;
                        }
                    }
                }

                $this->id    = $chunk;
                $this->name  = $chunk;
                $this->label = Utils::hyphenToSentence($chunk);

            } else {

                // Setting custom label if surrounded by double quotes
                if (!isset($chunk[0])) {
                    $breakme = 1;
                }
                if ($chunk[0] == '"' && substr($chunk, -1) == '"') {
                    $this->label = str_replace('"', '', $chunk);

                } else if ($chunk[0] == '_' && substr($chunk, -1) == '_') {
                    $this->helpText = Utils::removeFirstLastChar($chunk);

                } else if ($chunk[0] == '(' && substr($chunk, -1) == ')') {
                    $this->placeholder = Utils::removeFirstLastChar($chunk);

                } else if ($chunk[0] == '#' && substr($chunk, -1) == '#') {
                    $optionsString = Utils::removeFirstLastChar($chunk);;
                    $this->selectOptions = explode(',', $optionsString);
                    $this->selectOptions = array_map(function($v) { return trim($v); }, $this->selectOptions);
                    $this->type = 'select';
                }

            }
        }
    }


    public function setValue($value)
    {
        $this->value = $value;
        $this->validate($value);

        if (!empty($this->errors)) {
            return false;
        }
        return true;
    }

    protected function validate($value)
    {
        $errors = array();

        if ($this->isRequired && empty($value)) {
            $errors[] = $this->label.' is required';
        }

        if ($this->isConfirmationOf) {
            // TODO: Check value of comparison field
        }

        foreach ($this->validationTypes as $validationType) {
            $validationMethod = 'validate'.ucfirst(Utils::hyphenToCamel($validationType));

            if (method_exists($this, $validationMethod)) {
                $this->$validationMethod($value);
            }
        }

        if (!empty($errors)) {
            $this->hasError = true;
            $this->errors = $errors;
        }

        return $errors;
    }

    protected function validateEmail($value)
    {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = 'Not a valid email address';
        }
    }

    protected function validatePhone($value)
    {
        // TODO: Number formatting
    }

    protected function validatePassword($value)
    {
        $failed = false;

        if (strlen($value) < 8) {
            $failed = true;
        }

        if ($failed) {
            $this->errors[] = 'Password must be at least 8 characters long';
        }
    }

}

?>

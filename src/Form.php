<?php
/**
 * [Class Description]
 *
 * @author     John McCann
 */
 

namespace JMac\LazyForms;

use JMac\LazyForms\Field;
use JMac\LazyForms\FieldWriter;
//use JMac\Utils;

class Form
{
    public $classPrefix = 'form-';

    /** @var Field[] */
    public $fields = array ();

    /** @var array Hierarchical tree of fields used for drawing only */
    public $fieldTree = array();

    public $id = 'lazy-form';
    public $action = '#';
    public $method = 'post';
    public $class = 'form';

    public $labelSize;
    public $inputSize;

    public $tabIndex;
    public $hasError;

    protected $validData;
    protected $invalidData;

    public static function go (array $args, callable $onSuccess = null, callable $onFailure = null)
    {
        $form = new Form();

        if (array_key_exists('fields', $args)) {
            $formData = $args['fields'];
            unset($args['fields']);

            // Set general class properties from array key-value pairs
            foreach ($args as $key => $value) {
                if (property_exists($form, $key)) {
                    $form->$key = $value;
                }
            }

        } else if (!Utils::containsArray($args)) {
            // Args is just a list of field descriptions
            $formData = $args;

        }  else {
            throw new \Exception("Couldn't parse form fields!");
        }

        $form->populateFields($formData);
        $form->execute($onSuccess, $onFailure);
        return $form;
    }

    public function __construct ()
    {

    }

    public function populateFields (array $formData)
    {
        $this->fieldTree = array();
        $this->fields = array();

        $this->fieldTree = $this->parseFieldRow(null, $formData);
    }

    public function decodeRowConfiguration($key, $value)
    {
        if (is_numeric($key) ) {
            return [$value, '', ''];
        } else {
            if (is_array($value)) {
                return [$value, $key, ''];
            } else {
                return [$key, '', $value];
            }
        }
    }

    public function parseFieldRow($rowKey, $rowData)
    {
        $fields = [];
        foreach ($rowData as $key => $value) {
            list($fieldResult, $rowName) = $this->parseFieldData($key, $value);

            if ($rowName) {
                $fields[$rowName] = $fieldResult;
            } else {
                $fields[] = $fieldResult;
            }
        }
        return $fields;
    }

    protected function parseFieldData ($rowKey, $rowValue)
    {
        list($parsedData, $parsedTitle, $parsedValue) = $this->decodeRowConfiguration($rowKey, $rowValue);

        if (is_array($parsedData)) {

            $subFields = $this->parseFieldRow($parsedTitle, $parsedData);
            if ($parsedTitle ) {
                return [$subFields, $parsedTitle];
            } else {
                return [$subFields, null];
            }
        } else {
            $field = Field::create($parsedData, $parsedValue);
            $this->fields[$field->id] = $field;
            return [$field, null];
//            $this->fieldTree[] = $field;
        }
    }

    public function execute (callable $onSuccess = null, callable $onFailure = null)
    {
        $formWriter = FormWriter::create($this);

        if (!empty($_POST)) {
            $this->process($_POST);

            if ($this->hasError) {
                echo $formWriter->draw();
                if ($onFailure) {
                    call_user_func($onFailure, $this->validData);
                }

            } else if ($onSuccess) {
                call_user_func($onSuccess, $this->validData);

            } else {
                echo $formWriter->drawSuccessMessage();
            }

        } else {
            echo $formWriter->draw();
        }
    }

    public function process ($request)
    {
        $this->validData = array();
        $this->invalidData = array();

        foreach ($this->fields as $field) {
            if (isset($request[$field->id])) {
                $value = $request[$field->id];
                $valid = $field->setValue($value);
                if (!$valid) {
                    $this->hasError = true;
                    $this->invalidData[$field->id] = $value;
                } else {
                    $this->validData[$field->id] = $value;
                }
            }
        }

        if ($this->hasError) {
            return $this->invalidData;
        } else {
            return $this->validData;
        }
    }
}

?>

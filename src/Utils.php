<?php
/**
 * [Class Description]
 *
 * @author     JMac
 */


namespace JMac\LazyForms;


class Utils
{
    public static function isSetOr(&$var, $default = null)
    {
        return isset($var) ? $var : $default;
    }

    public static function isAssoc($array)
    {
        return (bool)count(array_filter(array_keys($array), 'is_string'));
    }


    public static function hyphenToCamel($string)
    {
        $parts = explode('-', $string);
        $parts = array_map('ucfirst', $parts);
        return lcfirst(implode('', $parts));
    }

    public static function hyphenToSentence($string)
    {
        return ucfirst(str_replace('-', ' ', $string));
    }

    public static function removeFirstLastChar($string)
    {
        return substr($string, 1, strlen($string) - 2);
    }

    public static function containsArray(array $array)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                return true;
            }
        }
        return false;
    }
}

?>
